package com.stripe.integration.service;

import com.stripe.integration.domain.Customer;
import com.stripe.integration.domain.InvoiceDetails;
import com.stripe.integration.repository.CustomerRepository;
import com.stripe.integration.repository.InvoiceDetailsRepository;
import com.stripe.model.Event;
import com.stripe.model.Invoice;
import com.stripe.model.StripeObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceService {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    InvoiceDetailsRepository invoiceDetailsRepository;

    public void handlePaidInvoice(Event event, StripeObject stripeObject) {
        Invoice invoice = (Invoice) stripeObject;
//        Customer newCustomer = customerRepository.findByStripeAccountId(invoice.getCustomer());
        Customer newCustomer = new Customer();
        newCustomer.setStripeAccountId(invoice.getCustomer());
        newCustomer.setLoggedInHours(0);
        newCustomer.setHourlyRate(20L);
        newCustomer.setCurrency("usd");
        InvoiceDetails invoiceDetails =new InvoiceDetails();
        invoiceDetails.setCustomer(newCustomer);
        invoiceDetails.setInvoiceUrl(invoice.getInvoicePdf());
        invoiceDetails.setInvoiceId(invoice.getId());
        invoiceDetails.setStatus(invoice.getStatus());
        customerRepository.save(newCustomer);
        invoiceDetailsRepository.save(invoiceDetails);
    }

    public void handleUnpaidInvoice(Event event, StripeObject stripeObject) {
        Invoice invoice = (Invoice) stripeObject;
//        Customer newCustomer = customerRepository.findByStripeAccountId(invoice.getCustomer());
        Customer newCustomer = new Customer();
        newCustomer.setStripeAccountId(invoice.getCustomer());
        newCustomer.setLoggedInHours(0);
        newCustomer.setHourlyRate(20L);
        newCustomer.setCurrency("usd");
        InvoiceDetails invoiceDetails =new InvoiceDetails();
        invoiceDetails.setCustomer(newCustomer);
        invoiceDetails.setInvoiceUrl(invoice.getInvoicePdf());
        invoiceDetails.setInvoiceId(invoice.getId());
        invoiceDetails.setStatus(invoice.getStatus());
        customerRepository.save(newCustomer);
        invoiceDetailsRepository.save(invoiceDetails);
    }
}
