package com.stripe.integration.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class InvoiceDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String invoiceUrl;

    @ManyToOne
    private Customer customer;

    private String invoiceId;

    private String status;
}
