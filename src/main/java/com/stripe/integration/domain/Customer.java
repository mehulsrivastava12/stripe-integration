package com.stripe.integration.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    private Integer loggedInHours;
    private Long hourlyRate;
    private String currency;
    private String stripeAccountId;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<TransferDetails> transferDetailsList = new HashSet<>();
    @OneToMany(cascade = CascadeType.ALL)
    private Set<InvoiceDetails> invoiceDetailsList = new HashSet<>();
}
