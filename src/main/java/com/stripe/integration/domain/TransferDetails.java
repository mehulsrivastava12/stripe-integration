package com.stripe.integration.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class TransferDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String transactionId;
    private Long amountTransfered;
    private String transactionStatus;
    private String paymentIntentId;
    @ManyToOne
    private Customer customer;
}
