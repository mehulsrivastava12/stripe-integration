package com.stripe.integration.repository;

import com.stripe.integration.domain.InvoiceDetails;
import com.stripe.integration.domain.TransferDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceDetailsRepository  extends JpaRepository<InvoiceDetails, Long> {


}
