package com.stripe.integration.repository;

import com.stripe.integration.domain.TransferDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransferDetailsRepository extends JpaRepository<TransferDetails, Long> {

    List<TransferDetails> getAllByTransactionStatusIsIn(String[] transactionStatus);
}
