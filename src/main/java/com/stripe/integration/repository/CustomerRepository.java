package com.stripe.integration.repository;

import com.stripe.integration.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    List<Customer> findAllByStripeAccountIdIsNotNullAndLoggedInHoursIsGreaterThan(Integer loggedInHour);

    Customer findByStripeAccountId(String accountId);
}
