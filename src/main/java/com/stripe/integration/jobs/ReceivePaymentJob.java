package com.stripe.integration.jobs;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.integration.domain.Customer;
import com.stripe.integration.domain.TransferDetails;
import com.stripe.integration.repository.CustomerRepository;
import com.stripe.integration.repository.TransferDetailsRepository;
import com.stripe.model.PaymentIntent;
import com.stripe.model.PaymentMethodCollection;
import com.stripe.param.CustomerListPaymentMethodsParams;
import com.stripe.param.PaymentIntentCreateParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.List;

@Slf4j
@Component
@EnableScheduling
class ReceivePaymentJob {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    TransferDetailsRepository transferDetailsRepository;

    @Value("${stripe.secret.api-key}")
    String apiKey;

//        @Scheduled(cron = "0 0 * * *")
//    @Scheduled(cron = "0 */1 * ? * *")
    public void receiveMoneyJob() throws StripeException {
        log.info("Receive Payment Job has started");
        Stripe.apiKey = apiKey;
        String paymentMethodId = null;
        List<Customer> customerToBePaid = customerRepository.findAllByStripeAccountIdIsNotNullAndLoggedInHoursIsGreaterThan(0);
        for(Customer customer:customerToBePaid){
            com.stripe.model.Customer resource = com.stripe.model.Customer.retrieve(customer.getStripeAccountId());
            CustomerListPaymentMethodsParams customerListPaymentMethodsParams =
                    CustomerListPaymentMethodsParams.builder().build();
            PaymentMethodCollection paymentMethods = resource.listPaymentMethods(customerListPaymentMethodsParams);
            if(paymentMethods.getData().get(0) != null){
                paymentMethodId = paymentMethods.getData().get(0).getId();
                System.out.println(paymentMethods.getData().get(0).getId());
                PaymentIntentCreateParams params =
                        PaymentIntentCreateParams.builder()
                                .setAmount(2000L)
                                .setCurrency(customer.getCurrency())
                                .setCustomer(customer.getStripeAccountId())
                                .setPaymentMethod(paymentMethodId)
                                .setConfirm(true)
                                .setReturnUrl("https://example.com/order/123/complete")
                                .build();
                PaymentIntent paymentIntent = PaymentIntent.create(params);
                PaymentIntent retrievePaymentIntent = PaymentIntent.retrieve(paymentIntent.getId());
                if (retrievePaymentIntent.getId() != null) {
                    TransferDetails transferDetails = new TransferDetails();
                    transferDetails.setAmountTransfered(retrievePaymentIntent.getAmount());
                    transferDetails.setTransactionId(retrievePaymentIntent.getId());
                    transferDetails.setCustomer(customer);
                    transferDetails.setPaymentIntentId(paymentIntent.getId());
                    transferDetails.setTransactionStatus(paymentIntent.getStatus());
                    transferDetailsRepository.save(transferDetails);
                    log.info("transfer money to customer stripe account" + customer.getStripeAccountId() + " is successful");
                }
                else {
                    log.info("transfer money to customer stripe account cannot be completed " + customer.getStripeAccountId());
                }
            }
        }
        log.info("Receive Payment Job has ended");
    }
}
