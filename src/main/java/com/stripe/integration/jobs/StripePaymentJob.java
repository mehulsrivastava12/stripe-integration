package com.stripe.integration.jobs;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.integration.domain.Customer;
import com.stripe.integration.domain.TransferDetails;
import com.stripe.integration.repository.CustomerRepository;
import com.stripe.integration.repository.TransferDetailsRepository;
import com.stripe.model.Account;
import com.stripe.model.Transfer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@EnableScheduling
class StripePaymentJob {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    TransferDetailsRepository transferDetailsRepository;

    @Value("${stripe.secret.api-key}")
    String apiKey;

//    @Scheduled(cron = "0 0 0   ?")
//    @Scheduled(cron = "0 * * * * ?")
    public void sendPaymentToCustomer() throws StripeException {
        Stripe.apiKey = apiKey;

        List<Customer> customerToBePaid = customerRepository.findAllByStripeAccountIdIsNotNullAndLoggedInHoursIsGreaterThan(0);
        for(Customer customer:customerToBePaid){
            Account account = Account.retrieve(customer.getStripeAccountId());
            String accountStatus = account.getIndividual().getVerification().getStatus();
            if (accountStatus.equals("verified") && account.getCapabilities().getTransfers().equals("active")) {
                Map<String, Object> params = new HashMap<>();
                params.put("amount", (customer.getHourlyRate()*customer.getLoggedInHours()*100));
                params.put("currency", "usd");
                params.put("destination", customer.getStripeAccountId());
                Transfer transfer = Transfer.create(params);
                if (transfer.getId() != null) {
                    customer.setLoggedInHours(0);
                    TransferDetails transferDetails = new TransferDetails();
                    transferDetails.setAmountTransfered(transfer.getAmount());
                    transferDetails.setTransactionId(transfer.getId());
                    transferDetails.setCustomer(customer);
                    transferDetailsRepository.save(transferDetails);
                    customerRepository.save(customer);
                    log.info("transfer money to customer stripe account" + customer.getStripeAccountId() + " is successful");
                }
                else {
                    log.info("transfer money to customer stripe account" + customer.getStripeAccountId() + " is not successful");
                }
            }
        }
    }
}
