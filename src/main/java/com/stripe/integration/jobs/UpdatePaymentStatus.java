package com.stripe.integration.jobs;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.integration.domain.Customer;
import com.stripe.integration.domain.InvoiceDetails;
import com.stripe.integration.domain.TransferDetails;
import com.stripe.integration.repository.CustomerRepository;
import com.stripe.integration.repository.InvoiceDetailsRepository;
import com.stripe.integration.repository.TransferDetailsRepository;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentRetrieveParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@EnableScheduling
class UpdatePaymentStatus {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    TransferDetailsRepository transferDetailsRepository;

    @Autowired
    InvoiceDetailsRepository invoiceDetailsRepository;

    @Value("${stripe.secret.api-key}")
    String apiKey;

//        @Scheduled(cron = "0 2 * * *")
//    @Scheduled(cron = "0 */2 * ? * *")
    public void updatePaymentStatus() throws StripeException {
        log.info("Job has started");
        Stripe.apiKey = apiKey;
        String paymentMethodId = null;
        String[] transactionStatus = {"processing","succeeded"};
        List<TransferDetails> transactionDetails = transferDetailsRepository.getAllByTransactionStatusIsIn(transactionStatus);
        for(TransferDetails transaction:transactionDetails){
            Map<String, Object> retrieveParams = new HashMap<>();
            retrieveParams.put("expand",new String[]{"latest_charge"});
            PaymentIntent paymentIntent = PaymentIntent.retrieve(transaction.getPaymentIntentId(),retrieveParams, null);
            if(paymentIntent.getStatus().equals("succeeded")){
                transaction.setTransactionStatus(paymentIntent.getStatus());
                transferDetailsRepository.save(transaction);
                Customer customer = transaction.getCustomer();
                customer.setLoggedInHours(0);
                InvoiceDetails invoiceDetails =new InvoiceDetails();
                invoiceDetails.setCustomer(customer);
                invoiceDetails.setInvoiceUrl(paymentIntent.getLatestChargeObject().getReceiptUrl());
                invoiceDetails.setInvoiceId(paymentIntent.getId());
                invoiceDetails.setStatus(paymentIntent.getStatus());
                invoiceDetailsRepository.save(invoiceDetails);
                customerRepository.save(customer);
            }
        }
        log.info("Job has ended");
    }
}
