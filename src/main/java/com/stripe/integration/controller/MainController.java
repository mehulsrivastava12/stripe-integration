package com.stripe.integration.controller;


import com.google.gson.Gson;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.integration.domain.Customer;
import com.stripe.integration.domain.InvoiceDetails;
import com.stripe.integration.repository.CustomerRepository;
import com.stripe.integration.repository.InvoiceDetailsRepository;
import com.stripe.model.*;
import com.stripe.param.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
public class MainController {

    @Value("${stripe.secret.api-key}")
    String apiKey;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    InvoiceDetailsRepository invoiceDetailsRepository;

    @RequestMapping("/")
    public String index() {
        return "paymentMethod";
    }

    @PostMapping("/connect-account")
    public ResponseEntity createConnectedAccount(HttpServletRequest request){
        Stripe.apiKey = apiKey;

        try {

            AccountCreateParams.TosAcceptance tosParams = AccountCreateParams.TosAcceptance.builder()
                    .setDate(new Date().getTime() / 1000)
                    .setIp(request.getRemoteAddr())
                    .build();

            Map<String, Object> bankAccount = new HashMap<>();
            bankAccount.put("country", "US");
            bankAccount.put("currency", "usd");
            bankAccount.put(
                    "account_holder_name",
                    "vifec"
            );
            bankAccount.put(
                    "account_holder_type",
                    "individual"
            );
            bankAccount.put("routing_number", "110000000");
            bankAccount.put("account_number", "000123456789");
            Map<String, Object> bankParams = new HashMap<>();
            bankParams.put("bank_account", bankAccount);

            Token token = Token.create(bankParams);


            AccountCreateParams params =
                    AccountCreateParams.builder()
                            .setType(AccountCreateParams.Type.CUSTOM)
                            .setCountry("US")
                            .setEmail("vifec68209@mcenb.com")
                            .setCapabilities(
                                    AccountCreateParams.Capabilities.builder()
                                            .setCardPayments(
                                                    AccountCreateParams.Capabilities.CardPayments.builder()
                                                            .setRequested(true)
                                                            .build()
                                            )
                                            .setTransfers(
                                                    AccountCreateParams.Capabilities.Transfers.builder()
                                                            .setRequested(true)
                                                            .build()
                                            )
                                            .build()
                            )
                            .setBusinessType(AccountCreateParams.BusinessType.INDIVIDUAL)
                            .setBusinessProfile(AccountCreateParams.BusinessProfile.builder().setUrl("http://www.test3.com")
                                    .setMcc("5734").build())
                            .setSettings(AccountCreateParams.Settings.builder().setPayments(AccountCreateParams.Settings.Payments.builder().setStatementDescriptor("http://www.test3.com").build()).build())
                            .setCompany(AccountCreateParams.Company.builder().setName("vifec's").setAddress(AccountCreateParams.Company.Address.builder().setLine1("test").setPostalCode("28105").setCity("Charlotte").setState("North Carolina").build()).setTaxId("000000000").build())
                            .setIndividual(AccountCreateParams.Individual.builder().setFirstName("vifec").setLastName("lanx").setDob(AccountCreateParams.Individual.Dob.builder().setDay(20L).setMonth(11L).setYear(2001L).build()).setAddress(AccountCreateParams.Individual.Address.builder().setLine1("test").setPostalCode("28105").setCity("Charlotte").setState("North carolina").build()).setEmail("vifec68209@mcenb.com").setPhone("(201) 555-0123").setSsnLast4("0000").setIdNumber("000000000").build())
                            .setExternalAccount(token.getId())
                            .setTosAcceptance(tosParams)
                            .build();
            Account account = Account.create(params);

            if (account.getId() != null) {
                Customer customer = new Customer();
                customer.setEmail(account.getEmail());
                customer.setName(account.getIndividual().getFirstName());
                customer.setStripeAccountId(account.getId());
                customer.setLoggedInHours(8);
                customer.setHourlyRate(20L);
                customerRepository.save(customer);
                return ResponseEntity.ok(customer);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Stripe Account cannot be created");
            }
        } catch (StripeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping("/add-bank-account")
    public void addBankAccount() {
        Stripe.apiKey = apiKey;

        try {
            // Replace with the actual customer ID
            String customerId = "cus_P88NuHAmuwQjWO";
            String microDeposit1 = "0.32";  // Replace with the actual amount
            String microDeposit2 = "0.45";

            // Create a token representing the bank account details
            Map<String, Object> bankAccount = new HashMap<>();
            bankAccount.put("country", "US");
            bankAccount.put("currency", "usd");
            bankAccount.put(
                    "account_holder_name",
                    "vifec"
            );
            bankAccount.put(
                    "account_holder_type",
                    "individual"
            );
            bankAccount.put("routing_number", "110000000");
            bankAccount.put("account_number", "000123456789");
            Map<String, Object> bankParams = new HashMap<>();
            bankParams.put("bank_account", bankAccount);

            Token token = Token.create(bankParams);

            // Attach the bank account to the customer
            com.stripe.model.Customer customer = com.stripe.model.Customer.retrieve(customerId);
            CustomerUpdateParams param = CustomerUpdateParams.builder().setSource(token.getId()).build();

            customer = customer.update(param);

            Map<String, Object> params = new HashMap<>();
            params.put("microdeposits", Arrays.asList(microDeposit1, microDeposit2));

//
//            customer.
//
//            System.out.println("Bank account added successfully to customer: " + customer.getId());
        } catch (StripeException e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/send")
    public void sendMoney() throws StripeException {
            Stripe.apiKey = apiKey;
            Balance balance = Balance.retrieve();
        PayoutCreateParams params =
                PayoutCreateParams.builder().setAmount(1100L).setCurrency("usd").setMethod(PayoutCreateParams.Method.INSTANT)
                        .setDestination("card_1Lg6cIAteVCukAWJLLD14GQd").build();
        Payout payout = Payout.create(params);
    }

    @PostMapping("/charge")
    public void charge() throws StripeException {
        Stripe.apiKey = apiKey;
        com.stripe.model.Customer resource = com.stripe.model.Customer.retrieve("cus_P8w5K8OXwMIZu4");
        CustomerListPaymentMethodsParams customerListPaymentMethodsParams =
                CustomerListPaymentMethodsParams.builder().setLimit(3L).build();
        PaymentMethodCollection paymentMethods = resource.listPaymentMethods(customerListPaymentMethodsParams);
        System.out.println(paymentMethods.getData().get(0).getId());
        PaymentIntentCreateParams params =
                PaymentIntentCreateParams.builder()
                        .setAmount(2000L)
                        .setCurrency("usd")
                        .setCustomer("cus_P8w5K8OXwMIZu4")
                        .setPaymentMethod("pm_1OKeDgAteVCukAWJivov5bc5")
                        .setConfirm(true)
                        .setReturnUrl("https://example.com/order/123/complete")
                        .build();
        PaymentIntent paymentIntent = PaymentIntent.create(params);
//        PaymentIntent retrievePaymentIntent = PaymentIntent.retrieve("pi_3OKe7JAteVCukAWJ0pGdnh46");
//        PaymentIntent confirmPaymentIntent = paymentIntent.confirm();
//        paymentIntent
    }

    @GetMapping("/invoices")
    private String getFinanceReports(Model model) {
        List<InvoiceDetails> invoiceDetails = invoiceDetailsRepository.findAll();
        model.addAttribute("invoiceList",invoiceDetails);
        return "financeReports";
    }
}
