package com.stripe.integration.controller;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.stripe.Stripe;
import com.stripe.exception.SignatureVerificationException;
import com.stripe.integration.domain.Customer;
import com.stripe.integration.domain.InvoiceDetails;
import com.stripe.integration.repository.CustomerRepository;
import com.stripe.integration.repository.InvoiceDetailsRepository;
import com.stripe.integration.service.InvoiceService;
import com.stripe.model.*;
import com.stripe.net.Webhook;
import com.stripe.param.CustomerCreateParams;
import com.stripe.param.SetupIntentCreateParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PaymentMethodController {

    @Value("${stripe.secret.api-key}")
    String apiKey;

    @Value("${stripe.webhook.secret}")
    private String endpointSecret;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    InvoiceDetailsRepository invoiceDetailsRepository;

    @Autowired
    InvoiceService invoiceService;
    @PostMapping("/create-customer")
    public String createCustomer() {
        Stripe.apiKey = apiKey;
        try {
            Gson gson = new Gson();
            CustomerCreateParams params =
                    CustomerCreateParams.builder()
                            .setName("Dua pipa")
                            .setEmail("dua@pipa.com")
                            .build();
            com.stripe.model.Customer customer = com.stripe.model.Customer.create(params);
            System.out.println("+++"+customer);
            if (customer.getId() != null) {
                Customer newCustomer = new Customer();
                newCustomer.setStripeAccountId(customer.getId());
                newCustomer.setEmail(customer.getEmail());
                newCustomer.setName(customer.getName());
                newCustomer.setLoggedInHours(10);
                newCustomer.setHourlyRate(20L);
                newCustomer.setCurrency("usd");
                customerRepository.save(newCustomer);
                SetupIntentCreateParams setupParams =
                        SetupIntentCreateParams
                                .builder()
                                .setCustomer(customer.getId()).setAutomaticPaymentMethods(SetupIntentCreateParams.AutomaticPaymentMethods.builder().setEnabled(true).build())
                                .build();

                SetupIntent setupIntent = SetupIntent.create(setupParams);

                Map<String, String> responseMap = new HashMap<>();
                responseMap.put("client_secret", setupIntent.getClientSecret());

                return gson.toJson(responseMap);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping("/webhook")
    public ResponseEntity<String> fetchEndpoint(@RequestBody String payload, @RequestHeader("Stripe-Signature") String sigHeader) {
        Stripe.apiKey = apiKey;
        try {
            Event event = Webhook.constructEvent(
                    payload, sigHeader, endpointSecret
            );

            // Deserialize the nested object inside the event
            EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
            StripeObject stripeObject = null;
            if (dataObjectDeserializer.getObject().isPresent()) {
                stripeObject = dataObjectDeserializer.getObject().get();
            }

            // Handle the event
            switch (event.getType()) {
                case "invoice.paid": {
                    invoiceService.handlePaidInvoice(event, stripeObject);
                    break;
                }
                case "invoice.payment_failed":
                    invoiceService.handleUnpaidInvoice(event, stripeObject);
                    break;
                case "payment_intent.created": {
                    System.out.println("++++"+stripeObject);
                    break;
                }
                default:
                    System.out.println("Unhandled event type: " + event.getType());
            }
            return ResponseEntity.status(HttpStatus.OK).body("Webhook received successfully.");
        } catch (SignatureVerificationException e) {
            // Invalid signature
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid webhook signature.");
        } catch (JsonSyntaxException e) {
            // Invalid payload
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid webhook payload.");
        } catch (Exception e) {
            // Handle other exceptions
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error processing webhook.");
        }
    }


}
